import { useRef } from "react";
import { Loginform } from "../login-form"
export function Login (){
    const ref = useRef(null)

    function login (values) {
        console.log(values)
        
    }

    return(
        <div className="container">
            <div className="row">
                <div className="col-6">
                </div>
                <div className="col-6">
                <Loginform onSubmit={login} innerRef={ref}/> 
                <button className="btn btn-info" 
                onClick={()=>{
                    ref.current.submitForm();
                }} 
                > Login </button>
                </div>
            </div>
            
        </div>
    )
}