import { Formik,Form,Field } from "formik";

export function Loginform ({onSubmit,innerRef}) {
    const inicialData = {
        user_name:"",
        password:"",
    }
    const submit = (values,actions) => {
        actions.setSubmitting(true);
        onSubmit(values);
        actions.resetForm();
        actions.setSubmitting(false);
    };

    return (
        <Formik initialValues={inicialData} onSubmit={submit} innerRef={innerRef}>
            {({isSubmitting})=> {
                return (<Form>
                     
                    <div className="form-group">
                    <label htmlFor="username">nombre de usuario</label>
                    <Field 
                    className='form-control'
                    id= 'username'
                    type='text'
                    name='user_name'
                    placeholder='ingrese su nombre de usuario'
                    />
                    </div>

                    
                    <div className="form-group">
                    <label htmlFor="pass">contrasena</label>
                    <Field 
                    className='form-control'
                    id= 'pass'
                    type='password'
                    name='password'
                    placeholder='ingrese su contrasena'
                    />
                    </div>                 
                                                                               
                </Form>
                )}}
        
        </Formik>
    )
}